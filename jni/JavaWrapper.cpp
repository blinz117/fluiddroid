#include <string.h>

#include "JavaWrapper.h"
#include "include/fluidsynth.h"
#include <android/log.h>

#define FLUID_SUCCESS 0

static fluid_synth_t* pSynth;
static fluid_settings_t* settings;
static fluid_audio_driver_t* adriver;
static fluid_sequencer_t* sequencer;
static fluid_player_t* player;


JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsInitSynth
  (JNIEnv * env, jobject obj)
  {
	__android_log_print(ANDROID_LOG_DEBUG, "BRYAN", "LOG IS WORKING!!!");
	 /* Create the settings object. This example uses the default
	 * values for the settings. */
	settings = new_fluid_settings();
	if (settings == NULL) {
			__android_log_print(ANDROID_LOG_INFO, "BRYAN", "failed to init settings");
			return;
	}

	/* Create the synthesizer */
	pSynth = new_fluid_synth(settings);
	if (pSynth == NULL) {
			__android_log_print(ANDROID_LOG_INFO, "BRYAN", "failed to init synth");
			return;
	}

	// TODO: Move initialization to its own method.
	player = new_fluid_player(pSynth);
	if (player == NULL) {
			__android_log_print(ANDROID_LOG_INFO, "BRYAN", "failed to init player");
			return;
	}

	fluid_settings_setint(settings, "audio.periods", 2);
	fluid_settings_setint(settings, "audio.period-size", 64);
	fluid_settings_setint(settings, "audio.realtime-prio", 1);
	fluid_settings_setstr(settings, "player.timing-source", "system");
  }


JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsDestroySynth
  (JNIEnv * env, jobject obj)
{
	if (player)
	{
		delete_fluid_player(player);
	}
    if (adriver) {
            delete_fluid_audio_driver(adriver);
    }
    if (pSynth) {
            delete_fluid_synth(pSynth);
    }
    if (settings) {
            delete_fluid_settings(settings);
    }
}


JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsLoadSoundfont
  (JNIEnv * env, jobject obj, jstring path)
  {
	const char *nativeString = env->GetStringUTFChars(path, 0);

	__android_log_print(ANDROID_LOG_INFO, "BRYAN", "loading %s", nativeString);

	/* Load the soundfont */
	int ret = fluid_synth_sfload(pSynth, nativeString, 1);
	if (ret == -1) // FLUIDSYNTH_FAIL
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "failed to load soundfont");
	}

	env->ReleaseStringUTFChars(path, nativeString);
  }


JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsNoteOn
  (JNIEnv * env , jobject obj, jint channel, jint key, jint velocity)
  {
	__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Note on!");
	int retCode = fluid_synth_noteon(pSynth, channel, key, velocity);
	if (retCode != FLUID_SUCCESS)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Note on failed!");
	}
  }


JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsNoteOff
  (JNIEnv * env, jobject obj, jint channel, jint key)
  {
	fluid_synth_noteoff(pSynth, channel, key);
  }

JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsFillBuffer
  (JNIEnv * env, jobject obj, jshortArray jbuffer, jint jbufLen)
{

	if (jbuffer == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Uh oh! Buffer is null!");
		return;
	}

	jshort* dst = env->GetShortArrayElements(jbuffer, 0);

	if (dst == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Uh oh! Native buffer is null!");
		return;
	}

	if (pSynth == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Uh oh! pSynth is null!");
		return;
	}

	if (env == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Uh oh! env is null!");
		return;
	}

	//the 0,2 and 1,2 are to interleave the stereo channels into one buffer
	int ret = fluid_synth_write_s16(pSynth, jbufLen/2, dst,0,2,dst,1,2);

	//if (!FLUID_SUCCESS)
			//__android_log_print(ANDROID_LOG_INFO, "BRYAN", "failed to write buffer");

	// Release the short* pointer
	env->ReleaseShortArrayElements(jbuffer, dst, 0);
	if (jbuffer = NULL)
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Oh shit!");
}

JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsPlayMIDIFile
  (JNIEnv * env, jobject obj, jstring path)
{
	if (player == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "Uh oh! player is null!");
		return;
	}

	const char *nativePath = env->GetStringUTFChars(path, 0);
	int pathLen = strlen(nativePath);
	char pathCopy[pathLen + 1];
	strcpy(pathCopy, nativePath);

	env->ReleaseStringUTFChars(path, nativePath);
	__android_log_print(ANDROID_LOG_INFO, "BRYAN", "loading %s", nativePath);

	if (!fluid_is_midifile(pathCopy))
	{
		__android_log_print(ANDROID_LOG_INFO, "BRYAN", "%s is not a MIDI file!", pathCopy);
	}
	else
	{
		fluid_player_add(player, pathCopy);
	    /* play the midi files, if any */
	    fluid_player_play(player);
	    /* wait for playback termination */
	    fluid_player_join(player);
	}
}

JNIEXPORT void JNICALL Java_com_blinz117_fluiddroid_FluidDroidSynth_fsStopPlayer
  (JNIEnv * env, jobject obj)
{
	fluid_player_stop(player);
	//fluid_player_join(player);
}
