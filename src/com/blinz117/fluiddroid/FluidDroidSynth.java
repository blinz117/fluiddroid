package com.blinz117.fluiddroid;

import android.media.*;
import android.os.Environment;
import android.util.Log;

public class FluidDroidSynth {

	static {
	    System.loadLibrary("fluidsynth-android");
	}
	
	AudioTrack m_Track;
	
	String m_currMIDIfile = "";
	 
	Thread m_streamThread = null;
	boolean m_bIsStreaming = true;
	
	boolean m_bIsPlayingNote = false;
	
	Runnable m_streamRunner = new Runnable()
	{
	    public void run()
	    {
	        int minBufferSize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
	        m_Track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize, AudioTrack.MODE_STREAM);
	        m_Track.play();
	        
	        int bufferSize = minBufferSize / 16;
	        Log.i("BRYAN", "Buffer size is " + bufferSize);
	        short[] streamBuffer = new short[bufferSize];

	        fsInitSynth();
	        String sdCardPath = Environment.getExternalStorageDirectory().getPath();
	        String filePath = sdCardPath + "/Fluidsynth/soundfont.sf2";
	        fsLoadSoundfont(filePath);
	        
	        while (!m_bIsStreaming)
	        {
	            // Have fluidSynth fill our buffer...
	        	fsFillBuffer(streamBuffer, bufferSize);

	        	// ... then feed that data to the AudioTrack
	        	m_Track.write(streamBuffer, 0, bufferSize);
	        }

	        m_Track.flush();
	        m_Track.stop();
	        
	        fsDestroySynth();
	    }
	};
	
	public void startSynth() {
		
		// not running yet
		if (m_bIsStreaming)
		{
			m_bIsStreaming = false;

			m_streamThread = new Thread(m_streamRunner);
			m_streamThread.setPriority(Thread.MAX_PRIORITY);
			m_streamThread.start();
		    
		    // need a small timeout to enable synth to start
		    try {
				Thread.sleep(1000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void destroy()
	{
		m_bIsStreaming = true;

		try
		{
		    m_streamThread.join();
		}
		catch(final Exception ex)
		{
		    ex.printStackTrace();
		}	
	}
	
	public void playMIDIFile(String path)
	{
		m_currMIDIfile = path;
		Thread playThread = new Thread(new Runnable() {
	        public void run() {
	        	m_bIsPlayingNote = true;
	        	fsPlayMIDIFile(m_currMIDIfile);
	        	m_bIsPlayingNote = false;
	        }
	    });
		playThread.setPriority(Thread.MAX_PRIORITY);
		playThread.start();
	}
	
	public void stopPlaying()
	{
		fsStopPlayer();
	}
	
	public void noteOn(int channel, int key, int velocity)
	{
		fsNoteOn(channel, key, velocity);
	}
	
	public void noteOff(int channel, int key)
	{
		fsNoteOff(channel, key);
	}
	
	private native void fsInitSynth();
	
	private native void fsDestroySynth();
	
	private native void fsLoadSoundfont(String path);
	
	private native void fsNoteOn(int channel, int key, int velocity);
	
	private native void fsNoteOff(int channel, int key);
	
	private native void fsFillBuffer(short[] buffer, int buffLen);
	
	private native void fsPlayMIDIFile(String path);
	
	private native void fsStopPlayer();
}
